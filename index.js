let http = require("http")

http.createServer(function(req, res) {
	
	if(req.url == "/profile" && req.method == "GET") {
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Welcome to your profile!")
	}
	else if(req.url == "/courses" && req.method == "GET") {
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Here’s our courses available")
	}
	else if(req.url == "/addCourse" && req.method == "POST") {
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Add a course to our resources")
	}
	else if(req.url == "/updateCourse" && req.method == "PUT") {
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Update a course to our resources")
	}
	else if(req.url == "/archiveCourse" && req.method == "DELETE") {
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Archive courses to our resources")
	}
	else {
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Welcome to Booking System")
	}

}).listen(4000)

console.log("Server is running at localhost:4000")